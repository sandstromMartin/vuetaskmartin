let app = new Vue({
	el: '#exchange',
	data () {
		return{		
			data: null,
			exg: "SEK",
			selectedFromCurrency: "SEK",
			selectedToCurrency: "USD",
			inputAmount: null,
			outputAmount: null,

		
		}
		
	},
	
	mounted () {
		axios
		  .get('https://api.exchangeratesapi.io/latest?')
		  .then(response => (this.data = response.data))
	  },
	created(){

	},
  	methods: {
		  clicked: function(value){
			  this.exg=value
			axios
			.get('https://api.exchangeratesapi.io/latest?base=' + this.exg)
			.then(response => (this.data = response.data))
		
		  },
		  write: function(){
			  let currency = {};
			  let t=this
			//  console.log(numIn)
			  axios
			.get('https://api.exchangeratesapi.io/latest?base='+t.selectedFromCurrency)
			
			.then(function (response) {
			
			currency = response.data.rates


			for( let key in currency){
				if(key == t.selectedToCurrency){
					t.outputAmount = (parseInt(t.inputAmount) * currency[key]);
				}

			}
			
				
			})
		
		  },
		  swap: function (from, to) {
			let t=this
			t.selectedFromCurrency=to
			t.selectedToCurrency=from

			t.write()

		
		  }
		 

		}

	  
})
